# Creating Chatbots - workshops

Warsztaty z tworzenia botów z użyciem Microsoft Azure

## Zastosowanie bota

Nasz chatbot będzie wyszukiwać darmowe książki na podstawie wpisanego tytułu lub autora oraz będzie zwracać informacje
o autrach, epokach i gatunkach literackich.

## Harmonogram

* 9:00 - 9:45 - wstęp teoretyczny
* 9:45 - 10:00 - przerwa
* 10:00 - 11:00 - bot cz. 1
* 11:00 - 11:15 - przerwa
* 11:15 - 12:15 - bot cz. 2
* 12:15 - 12:30 - przerwa
* 12:30 - 13:30 - bot cz. 3

## Funkcjonalności bota

* powitanie - co chcesz zrobić?
* wyszukiwanie książek - po tytule lub autorze
* zwracanie informacji o epoce lub autorze lub gatunku literackim
* pomoc, wyjście

## Instrukcja

### Założenie projektu

Bot Framework Composer: http://bot-composer.westeurope.cloudapp.azure.com:3000/ 

W oknie głównym Composera klikamy "+ New".

![Image](images/new_project.png)

W nowym oknie mamy do wyboru utworzenie bota od podstaw, z użyciem QnA Makera i z szablonu. Wybieramy "Create from scratch" i "Next".

![Image](images/new_project2.png)

Następnie musimy wprowadzić nazwę naszego bota, jego lokalizację (obojętne) i ewentualnie krótki opis.

![Image](images/new_project3.png)

Utworzyliśmy nowy projekt. Okno Composera powinno wyglądać tak:

![Image](images/new_project4.png)

Po lewej stronie mamy panel nawigacyjny (Navigation), na środku tzw. Authoring canvas i panel właściwości (Properties) po prawej.

W panelu nawigacyjnym możemy zobaczyć, że został automatycznie utworzony tzw. trigger Greeting. 
TRIGGER to zdarzenie, które wyzwala jakąś akcję - tutaj to słowa, które przekierowują 'flow' konwersacji na jakąś ścieżkę.
Program automatycznie utworzył nam standardowy flow na powitanie.

![Image](images/greeting.png)

We flow możemy dodawać tzw. akcje symbolem plusika pomiędzy wszystkimi bloczkami. 

Nazwę 'Greeting' można zmienić na np. 'Welcome User' w panelu właściwości po prawej stronie.

![Image](images/welcome_user.png)

W bloczku 'Send a response' (wewnątrz warunku i gałęzi 'True') wpiszmy wiadomość, jaką chcemy, żeby bot wysyłał na samym początku.

![Image](images/welcome_user1.png)

Mamy przywitanie, teraz przetestujmy, czy bot się włącza. Kliknijcie w przycisk na samej górze z prawej strony - 'Start Bot'.
Po jakimś czasie przycisk zmieni się na 'Restart Bot' i pojawi się przycisk 'Test in Emulator'.

![Image](images/test_bot.png)

Następnie klikamy 'Test in Emulator'. Powinien się włączyć nowy program - Bot Framework Emulator. Po załadowaniu bota wyświetli się
od razu nasza powitalna wiadomość:

![Image](images/test_bot1.png)

Po lewej stronie mamy okno czatu, a po prawej logi. Na dole są wypisane krok po kroku wszystkie operacje wykonywane przez emulator,
jeśli klikniemy w któryś z elementów, szczegóły operacji wyświetlą się powyżej.


### Dodawanie nowych dialogów

Plan konwersacji z naszym botem będziemy grupować w tzw. dialogi. DIALOGI to fragmenty konwersacji zgrupowane względem
funkcjonalności, np. w naszym projekcie utworzymy 3 podstawowe dialogi:
* szukanie książek
* wyszukiwanie informacji o epoce
* wyszukiwanie informacji o autorze

Dialogi tworzymy klikając '+ Add' na górze Authoring Canvas i następnie 'Add new dialog'.


## Przydatne linki

https://docs.microsoft.com/en-us/composer/tutorial/tutorial-create-bot - tutorial tworzenia bota pogodowego

https://gitlab.com/malwinq/ai-on-microsoft-azure/-/tree/master/Bot - mój bot wyszukujący przepisy i opowiadający suchary

---------------------------

### Notatki 

sprawdzić czy mogę udostępnić jeden monitor

1. Bot Framework Composer
2. Inne możliwości utworzenia bota: QnA Maker, szablon
3. Triggery
4. Pętla, warunek w greeting
5. Język angielski




Książki

- powitanie
- co chcesz zrobić

U książka
- po czym chcesz wyszukać?
U tytuł / autor
- książka, dane, link

U epoki
- jakiej epoki szukasz?
U epoka
- dane / nie znaleziono, lista epok:
- epoki

U autor
- jakiego autora szukasz?
U autor
- dane / nie znaleziono, wpisz z -



1. Powitanie
2. Trigger książka
3. Książka: zamockowane dane

4. Trigger autor
5. Autor: zamockowane dane

6. REST API
7. Dodanie do książki
8. Błędy

9. Dodanie do autora - samodz.

10. Trigger epoki
11. REST epoki
12. wyświetlanie
13. błąd, lista epok

14. pomoc
15. wyjście

16. LUIS
